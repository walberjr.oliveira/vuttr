package br.com.walloliveira.vuttr;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Application;

import static org.assertj.core.api.Assertions.assertThat;

public class ApiApplicationTest {

	@Test
	@DisplayName(
		"Given that a new instance of ApiApplication class is created" +
			"then the object created must be of Application type"
	)
	void testApiApplication() {
		var application = new ApiApplication();
		assertThat(application).isInstanceOf(Application.class);
	}
}
