package br.com.walloliveira.vuttr.utils.valueObjects.url;

import br.com.walloliveira.vuttr.utils.valueObjects.url.exceptions.URLValueIsInvalidException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UrlVOTest {

	@Test
	@DisplayName(
		"Given that the url value is valid " +
			"when the 'of' method is called of UrlVO Class " +
			"then a new UrlVO instance must be created"
	)
	void testCreateNewInstanceUrlVO() {
		var urlVO = UrlVO.of("https://www.google.com");

		assertThat(urlVO.getLink()).isEqualTo("https://www.google.com");
	}

	@Test
	@DisplayName(
		"Given that the url value is invalid " +
			"when the 'of' method is called of UrlVO Class " +
			"then an error must be thrown "
	)
	void testTryCreateNewInstanceUrlVO() {
		Exception exception = assertThrows(
			URLValueIsInvalidException.class,
			() -> UrlVO.of("htp:/invalid.com")
		);
		assertThat(exception.getMessage()).isEqualTo("The link value is invalid: htp:/invalid.com");
	}
}
