package br.com.walloliveira.vuttr;

import java.util.stream.Stream;

import org.junit.jupiter.params.provider.Arguments;
import org.mockito.Mockito;

import br.com.walloliveira.vuttr.resources.tools.requests.CreateToolsRequest;

public class MethodResourceTest {
	public static Stream<Arguments> request() {
		return Stream.of(
			Arguments.arguments(
				Mockito.mock(CreateToolsRequest.class)
			)
		);
	}
}
