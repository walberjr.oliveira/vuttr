package br.com.walloliveira.vuttr.exceptionMappers;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import br.com.walloliveira.vuttr.domains.tools.exceptions.TagValueIsNullOrEmptyException;

public class TagValueIsNullOrEmptyExceptionMapperTest {

	@ParameterizedTest
	@MethodSource("exception")
	@DisplayName(
		"Given that the toResponse method of instance from TagValueIsNullOrEmptyExceptionMapper is called" +
			"then the response status must be equals 400" +
			"and the entity body must be a ResponseError instance"
	)
	void testMethodToResponseOfTagValueIsNullOrEmptyExceptionMapper(TagValueIsNullOrEmptyException exception) {
		var exceptionMapper = new TagValueIsNullOrEmptyExceptionMapper();
		var response = exceptionMapper.toResponse(exception);
		assertThat(response.getStatus()).isEqualTo(400);
		assertThat(response.getEntity()).isInstanceOf(ResponseError.class);
	}

	static Object[] exception() {
		return new Object[]{
			new TagValueIsNullOrEmptyException()
		};
	}
}
