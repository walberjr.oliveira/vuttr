package br.com.walloliveira.vuttr.exceptionMappers;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import br.com.walloliveira.vuttr.utils.valueObjects.url.exceptions.URLValueIsInvalidException;

public class URLValueIsInvalidExceptionMapperTest {

	@ParameterizedTest
	@MethodSource("exception")
	@DisplayName(
		"Given that the toResponse method of instance from URLValueIsInvalidExceptionMapper is called" +
			"then the response status must be equals 422" +
			"and the entity body must be a ResponseError instance"
	)
	void testMethodToResponseOfURLValueIsInvalidExceptionMapper(URLValueIsInvalidException exception) {
		var exceptionMapper = new URLValueIsInvalidExceptionMapper();
		var response = exceptionMapper.toResponse(exception);
		assertThat(response.getStatus()).isEqualTo(422);
		assertThat(response.getEntity()).isInstanceOf(ResponseError.class);
	}

	static Object[] exception() {
		return new Object[]{
			new URLValueIsInvalidException("invalid")
		};
	}
}
