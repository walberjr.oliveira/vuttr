package br.com.walloliveira.vuttr.exceptionMappers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ResponseErrorTest {

	@Test
	@DisplayName(
		"Given that the parameter is valid " +
			"when the 'of' method of the ResponseError class is called" +
			"then the object of ResponseError type must be instantiate"
	)
	void testCreateInstanceResponseError() {
		var responseError = ResponseError.of("Message Error");
		assertThat(responseError.message).isEqualTo("Message Error");
	}
}
