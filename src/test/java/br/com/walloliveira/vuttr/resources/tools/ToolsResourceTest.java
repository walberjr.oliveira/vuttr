package br.com.walloliveira.vuttr.resources.tools;

import br.com.walloliveira.vuttr.TypeTest;
import br.com.walloliveira.vuttr.domains.tools.ToolsRepository;
import br.com.walloliveira.vuttr.resources.tools.requests.CreateToolsRequest;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.notNullValue;

@QuarkusTest
@Tag(TypeTest.INTEGRATION)
@Transactional
public class ToolsResourceTest {
	private final ToolsRepository toolsRepository;

	@Inject
	public ToolsResourceTest(ToolsRepository toolsRepository) {
		this.toolsRepository = toolsRepository;
	}

	@BeforeEach
	public void setUp() {
		this.toolsRepository.deleteAll();
	}

	@Test
	@DisplayName(
		"Given that the tools request is valid " +
			"when the method POST on endpoint '/tools' is called " +
			"then the status code must be 201 " +
			"and the response must be a tools"
	)
	public void testPostToolsResource() {
		CreateToolsRequest payload = create_payload();
		given()
			.body(payload)
			.contentType(ContentType.JSON)
			.when()
			.post("/tools")
			.then()
			.assertThat()
			.statusCode(201)
			.contentType(ContentType.JSON)
			.body("link", is("http://localhost:1010"))
			.body("title", is("New Test Tools"))
			.body("id", is(notNullValue()))
			.body("description", is("An new test tools"))
			.body("tags", is(List.of("node", "java")));
	}

	@Test
	@DisplayName(
		"Given that the tools request is valid " +
			"and the method POST on endpoint '/tools' is called " +
			"and the status code is 201 " +
			"when the method get on endpoint '/tools' is called" +
			"then the status code must be 200 " +
			"and the response body must be a tools list"
	)
	public void testGetToolsResource() {
		var payload = create_payload();
		given()
			.contentType(ContentType.JSON)
			.body(payload)
			.when()
			.post("tools")
			.then()
			.statusCode(201);
		var toolsList = given()
			                .contentType(ContentType.JSON)
			                .when()
			                .get("/tools")
			                .as(List.class, ObjectMapperType.JSONB);
		assertThat(toolsList).hasSize(1);
		Map<String, Object> actual = (Map<String, Object>) toolsList.get(0);
		assertThat(actual.get("id")).isNotNull();
		assertThat(actual.get("title")).isEqualTo("New Test Tools");
		assertThat(actual.get("link")).isEqualTo("http://localhost:1010");
		assertThat(actual.get("description")).isEqualTo("An new test tools");
		List<String> tags = (List<String>) actual.get("tags");
		assertThat(tags).hasSize(2);
		assertThat(tags).contains("java", "node");
	}

	@Test
	@DisplayName(
		"Given that the tools request is valid " +
			"and the method POST on endpoint '/tools' is called " +
			"and the status code is 201 " +
			"when the method GET on endpoint '/tools' is called " +
			"and the query string name tag has php value " +
			"then the status code must be 200 " +
			"and the response body must be a empty tools list"
	)
	public void testGetToolsResourceWithQueryString() {
		var payload = create_payload();
		given()
			.contentType(ContentType.JSON)
			.body(payload)
			.when()
			.post("tools")
			.then()
			.statusCode(201);
		var toolsList = given()
			                .contentType(ContentType.JSON)
			                .when()
			                .get("/tools?tag=php")
			                .as(List.class, ObjectMapperType.JSONB);
		assertThat(toolsList).isEmpty();
	}

	@NotNull
	private CreateToolsRequest create_payload() {
		var payload = new CreateToolsRequest();
		payload.title = "New Test Tools";
		payload.link = "http://localhost:1010";
		payload.description = "An new test tools";
		payload.tags = Set.of("java", "node");
		return payload;
	}
}

