package br.com.walloliveira.vuttr.domains.tools;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.Set;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import br.com.walloliveira.vuttr.utils.valueObjects.url.UrlVO;

public class ToolsTest {

	@ParameterizedTest
	@MethodSource("urlVOAndTags")
	@DisplayName(
		"Given that the parameters are valid" +
			"when the 'of' method of Tools class is called" +
			"then the object of Tools class must be created"
	)
	void testCreateToolsInstance(UrlVO urlVO, Set<TagVO> tags) {
		var tools = Tools.of(
			urlVO,
			"Title",
			"Description",
			tags
		);

		assertThat(tools.getTitle()).isEqualTo("Title");
		assertThat(tools.getDescription()).isEqualTo("Description");
		assertThat(tools.getLink()).isEqualTo("http://localhost:8888/test");
		assertThat(tools.getTags()).hasSize(2);
		assertThat(tools.getTags()).isEqualTo(Set.of("node", "java"));
	}

	@ParameterizedTest
	@MethodSource("urlVOAndTags")
	@DisplayName(
		"Given that the parameters are valid" +
			"when the 'removeTags' method of Tools class is called" +
			"then the tags of Tools must be empty"
	)
	void testRemoveTagsOfToolsInstance(UrlVO urlVO, Set<TagVO> tags) {
		var tools = Tools.of(
			urlVO,
			"Title",
			"Description",
			tags
		);

		tools.removeTags();
		assertThat(tools.getTags()).isEmpty();
	}

	static Stream<Arguments> urlVOAndTags() {
		return Stream.of(
			arguments(
				UrlVO.of("http://localhost:8888/test"),
				Set.of(
					TagVO.of("node"),
					TagVO.of("java")
				)
			),
			arguments(
				UrlVO.of("http://localhost:8888/test"),
				Set.of(
					TagVO.of("node"),
					TagVO.of("java")
				)
			)
		);
	}
}
