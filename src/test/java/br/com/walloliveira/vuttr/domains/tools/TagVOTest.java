package br.com.walloliveira.vuttr.domains.tools;

import br.com.walloliveira.vuttr.domains.tools.exceptions.TagValueIsNullOrEmptyException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TagVOTest {
	@Test
	@DisplayName(
		"Given that the parameter is valid" +
			"when the 'of' method of TagVO class is called" +
			"then the object of Tools class must be created"
	)
	void testCreateTagVOInstance() {
		var tagVO = TagVO.of("node");
		assertThat(tagVO.getValue()).isEqualTo("node");
	}

	@Test
	@DisplayName(
		"Given that the parameter is valid " +
			"when the 'of' method of TagVO class is called " +
			"then the error must be thrown"
	)
	void testTryCreateIncorrectTagVOInstance() {
		var exception = assertThrows(
			TagValueIsNullOrEmptyException.class,
			() -> TagVO.of("")
		);
		assertThat(exception.getMessage()).isEqualTo("Tag is required");
	}
}
