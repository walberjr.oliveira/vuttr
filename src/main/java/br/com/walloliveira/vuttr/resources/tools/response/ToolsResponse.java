package br.com.walloliveira.vuttr.resources.tools.response;

import br.com.walloliveira.vuttr.domains.tools.Tools;

import java.io.Serializable;
import java.util.Set;

public class ToolsResponse implements Serializable {
	public final String link;
	public final Long id;
	public final String description;
	public final Set<String> tags;
	public final String title;

	public ToolsResponse(
		Long id,
		String link,
		String description,
		Set<String> tags,
		String title
	) {
		this.link = link;
		this.id = id;
		this.description = description;
		this.tags = tags;
		this.title = title;
	}

	public static ToolsResponse fromDomain(final Tools tools) {
		return new ToolsResponse(
			tools.id,
			tools.getLink(),
			tools.getDescription(),
			tools.getTags(),
			tools.getTitle()
		);
	}

}
