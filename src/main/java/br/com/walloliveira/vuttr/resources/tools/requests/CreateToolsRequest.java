package br.com.walloliveira.vuttr.resources.tools.requests;

import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.com.walloliveira.vuttr.domains.tools.TagVO;
import br.com.walloliveira.vuttr.domains.tools.Tools;
import br.com.walloliveira.vuttr.utils.valueObjects.url.UrlVO;

public class CreateToolsRequest {

	@NotNull(message = "Title is required")
	@NotEmpty(message = "Title is required")
	public String title;

	public String description;

	@NotNull(message = "Link is required")
	@NotEmpty(message = "Link is required")
	public String link;

	@NotNull(message = "Tags is required")
	@NotEmpty(message = "Tags size must be 1 element")
	public Set<String> tags;

	public Tools toDomain() {
		return Tools.of(
			UrlVO.of(this.link),
			this.title,
			this.description,
			this.tags
				.stream()
				.map(TagVO::of)
				.collect(Collectors.toSet())
		);
	}
}
