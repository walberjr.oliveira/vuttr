package br.com.walloliveira.vuttr.resources.tools;

import br.com.walloliveira.vuttr.domains.tools.TagVO;
import br.com.walloliveira.vuttr.domains.tools.Tools;
import br.com.walloliveira.vuttr.domains.tools.ToolsRepository;
import br.com.walloliveira.vuttr.resources.tools.requests.CreateToolsRequest;
import br.com.walloliveira.vuttr.resources.tools.response.ToolsResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.base.Strings.isNullOrEmpty;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.OK;

@Path("/tools")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Tools")
public class ToolsResource {

	private ToolsRepository toolsRepository;

	public ToolsResource() {
	}

	@Inject
	ToolsResource(ToolsRepository toolsRepository) {
		this();
		this.toolsRepository = toolsRepository;
	}

	@Transactional
	@POST
	public Response post(@Valid final CreateToolsRequest request) {
		var domain = request.toDomain();
		toolsRepository.persist(domain);
		var response = ToolsResponse.fromDomain(domain);
		return Response.status(CREATED)
			       .entity(response)
			       .build();
	}

	@GET
	public Response get(@QueryParam("tag") String tag) {
		List<Tools> toolsList;
		if (isNullOrEmpty(tag)) {
			toolsList = toolsRepository.listAll();
		} else {
			toolsList = toolsRepository.findByTag(TagVO.of(tag));
		}
		return Response.status(OK)
			       .entity(
				       toolsList.stream()
					       .map(ToolsResponse::fromDomain)
					       .collect(Collectors.toList()))
			       .build();
	}
}
