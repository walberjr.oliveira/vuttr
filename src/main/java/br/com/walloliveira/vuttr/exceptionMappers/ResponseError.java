package br.com.walloliveira.vuttr.exceptionMappers;

import org.jetbrains.annotations.NotNull;

public final class ResponseError {
	public final String message;

	private ResponseError(final String message) {
		this.message = message;
	}

	public static ResponseError of(@NotNull final String message) {
		return new ResponseError(message);
	}
}
