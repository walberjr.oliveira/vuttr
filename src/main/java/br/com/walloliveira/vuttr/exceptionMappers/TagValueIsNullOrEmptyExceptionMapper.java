package br.com.walloliveira.vuttr.exceptionMappers;

import br.com.walloliveira.vuttr.domains.tools.exceptions.TagValueIsNullOrEmptyException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

@Provider
public final class TagValueIsNullOrEmptyExceptionMapper implements ExceptionMapper<TagValueIsNullOrEmptyException> {
	public TagValueIsNullOrEmptyExceptionMapper() {}

	@Override
	public Response toResponse(TagValueIsNullOrEmptyException e) {
		return Response.status(BAD_REQUEST)
			       .entity(ResponseError.of(e.getMessage()))
			       .build();
	}
}
