package br.com.walloliveira.vuttr.exceptionMappers;

import br.com.walloliveira.vuttr.utils.valueObjects.url.exceptions.URLValueIsInvalidException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public final class URLValueIsInvalidExceptionMapper implements ExceptionMapper<URLValueIsInvalidException> {
	private static final int UNPROCESSABLE_ENTITY = 422;

	public URLValueIsInvalidExceptionMapper() {}

	@Override
	public Response toResponse(URLValueIsInvalidException e) {
		return Response.status(UNPROCESSABLE_ENTITY)
			       .entity(ResponseError.of(e.getMessage()))
			       .build();
	}
}
