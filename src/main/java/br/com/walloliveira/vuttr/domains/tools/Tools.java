package br.com.walloliveira.vuttr.domains.tools;

import br.com.walloliveira.vuttr.utils.valueObjects.url.UrlVO;
import com.google.common.collect.Sets;
import io.quarkus.hibernate.orm.panache.PanacheEntity;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Tools extends PanacheEntity {

	@AttributeOverrides(
		@AttributeOverride(
			name = "link",
			column = @Column(name = "link")
		)
	)
	private UrlVO link;

	@Column(name = "title")
	private String title;

	@Column(name = "description", length = 2048)
	private String description;

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(
		name = "tools_tags",
		joinColumns = @JoinColumn(name = "tools_id")
	)
	private Set<TagVO> tags = Sets.newHashSet();

	Tools() {
	}

	private Tools(
		final UrlVO link,
		final String title,
		final String description,
		final Set<TagVO> tags
	) {
		this();
		this.link = link;
		this.title = title;
		this.description = Optional.ofNullable(description)
			                   .orElse("");
		this.tags.addAll(tags);
	}

	public static Tools of(
		@NotNull final UrlVO link,
		@NotNull final String title,
		final String description,
		@NotNull final Set<TagVO> tags
	) {
		return new Tools(
			link,
			title,
			description,
			tags
		);
	}

	public String getLink() {
		return this.link.getLink();
	}

	public Set<String> getTags() {
		return this.tags
			       .stream()
			       .map(TagVO::getValue)
			       .collect(Collectors.toSet());
	}

	public String getDescription() {
		return description;
	}

	public String getTitle() {
		return title;
	}

	void removeTags() {
		this.tags.clear();
	}

}
