package br.com.walloliveira.vuttr.domains.tools.exceptions;

public final class TagValueIsNullOrEmptyException extends RuntimeException {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public TagValueIsNullOrEmptyException() {
		super("Tag is required");
	}
}
