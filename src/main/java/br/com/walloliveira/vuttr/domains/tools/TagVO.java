package br.com.walloliveira.vuttr.domains.tools;

import br.com.walloliveira.vuttr.domains.tools.exceptions.TagValueIsNullOrEmptyException;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import static com.google.common.base.Strings.isNullOrEmpty;

@Embeddable
public final class TagVO {
	@Column(name = "value")
	private String value;

	TagVO() {

	}

	public String getValue() {
		return value;
	}

	private TagVO(final String tag) {
		this();
		if (isNullOrEmpty(tag)) {
			throw new TagValueIsNullOrEmptyException();
		}
		this.value = tag;
	}

	public static TagVO of(final String tag) {
		return new TagVO(tag);
	}
}
