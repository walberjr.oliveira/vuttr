package br.com.walloliveira.vuttr.domains.tools;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class ToolsRepository implements PanacheRepository<Tools> {

	@Override
	public long deleteAll() {
		this.listAll()
			.stream()
			.peek(Tools::removeTags)
			.forEach(Tools::delete);
		return 0;
	}

	public List<Tools> findByTag(TagVO tag) {
		return list("from Tools tools " +
			            "join tools.tags as tag " +
			            "where tag.value = ?1", tag.getValue());
	}
}