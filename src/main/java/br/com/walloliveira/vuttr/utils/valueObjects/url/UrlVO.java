package br.com.walloliveira.vuttr.utils.valueObjects.url;

import br.com.walloliveira.vuttr.utils.valueObjects.url.exceptions.URLValueIsInvalidException;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Embeddable;
import java.net.MalformedURLException;
import java.net.URL;

@Embeddable
public final class UrlVO {
	private String link;

	UrlVO() {
	}

	private UrlVO(String link) {
		this();
		try {
			var url = new URL(link);
			this.link = url.toString();
		} catch (MalformedURLException e) {
			throw new URLValueIsInvalidException(link);
		}
	}

	public String getLink() {
		return link;
	}

	public static UrlVO of(@NotNull String value) {
		return new UrlVO(value);
	}
}
